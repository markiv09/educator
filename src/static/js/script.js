$(document).ready(function(){
	var filters = [
		{id: 1, value: "Mathematics", video: "1.mp4"},
		{id: 2, value: "Sanksrit", video: "2.mp4"},
		{id: 3, value: "Literature", video: "3.mp4"},
		{id: 4, value: "Thermodynamics", video: "4.mp4"},
		{id: 5, value: "Physics", video: "5.mp4"},
		{id: 6, value: "Logic", video: "6.mp4"},
	]
	
	var course_catalog = [
		{name: "Linear Algebra for Computer Applications", author: "Amish B"},
		{name: "Linear Regression and Data Modelling", author: "Bipinchandra"},
		{name: "Introduction to Politics", author: "Chaanakya"},
		{name: "Building Dynamic Web Pages", author: "Dean Jones"},
		{name: "State Board IV", author: "State Board"},
		{name: "State Board V", author: "State Board"},
		{name: "State Board VI", author: "State Board"},
		{name: "State Board VII", author: "State Board"},
		{name: "State Board VIII", author: "State Board"},
		{name: "State Board IX", author: "State Board"},
	]

	// $('#videoHolder').children().hide()
	
	$.each(filters, function () {
    $("#filtersDiv").append($("<div>").addClass("checkbox").append($("<label>").text(this.value).prepend(
        $("<input>").attr('type', 'checkbox').attr('id',this.value).prop('checked', this.checked))
		));
	});
	
	$.each(course_catalog, function () {
    $("#catalog").append($("<div>").addClass("col-md-6 col-lg-6 jumbotron bordering_for_catalog").append($("<div>").addClass("row").append($("<div>")).append($("<div>").append($("<p>").addClass("text-primary").text(this.name)).append($("<p>").addClass("text-muted").text("- "+this.author)))));
	});
	
	$(".checkbox").click(function(){
		
		var array_of_all_checked_checkboxes = []
		var array_of_all_unchecked_checkboxes = []
		
		$(this).find('input:checkbox').each(function () {
			this.checked ? array_of_all_checked_checkboxes.push($(this).attr('id')) : array_of_all_unchecked_checkboxes.push($(this).attr('id'));
		});

		$.each(array_of_all_unchecked_checkboxes, function(i, value){
			$('#videoHolder').children().find('#'+value).parent().hide()
		});

		$.each(array_of_all_checked_checkboxes, function(i, value){
			$('#videoHolder').children().find('#'+value).parent().show()
		});
		
	})
	
});
